#!/bin/sh

cargo install grpc-compiler
cargo install protobuf-codegen --version 2.18.2

cd $HOME/src/github.com/hyperledger/fabric-protos

protoc --rust_out=$HOME/src/rust/crypto/fabric-protos-rust/src/common/ common/collection.proto common/configtx.proto common/ledger.proto common/common.proto common/configuration.proto common/policies.proto

protoc --rust_out=$HOME/src/rust/crypto/fabric-protos-rust/src/discovery/ discovery/protocol.proto
protoc --rust-grpc_out=$HOME/src/rust/crypto/fabric-protos-rust/src/discovery/ discovery/protocol.proto

protoc --rust_out=$HOME/src/rust/crypto/fabric-protos-rust/src/gateway/ gateway/gateway.proto
protoc --rust-grpc_out=$HOME/src/rust/crypto/fabric-protos-rust/src/gateway/ gateway/gateway.proto

protoc --rust_out=$HOME/src/rust/crypto/fabric-protos-rust/src/gossip/ gossip/message.proto
protoc --rust-grpc_out=$HOME/src/rust/crypto/fabric-protos-rust/src/gossip/ gossip/message.proto

protoc --rust_out=$HOME/src/rust/crypto/fabric-protos-rust/src/msp msp/identities.proto msp/msp_config.proto msp/msp_principal.proto

protoc --rust_out=$HOME/src/rust/crypto/fabric-protos-rust/src/ledger/queryresult ledger/queryresult/kv_query_result.proto
protoc --rust_out=$HOME/src/rust/crypto/fabric-protos-rust/src/ledger/rwset ledger/rwset/rwset.proto
protoc --rust_out=$HOME/src/rust/crypto/fabric-protos-rust/src/ledger/rwset/kvrwset ledger/rwset/kvrwset/kv_rwset.proto

protoc --rust_out=$HOME/src/rust/crypto/fabric-protos-rust/src/orderer/ orderer/ab.proto orderer/configuration.proto orderer/cluster.proto
protoc --rust-grpc_out=$HOME/src/rust/crypto/fabric-protos-rust/src/orderer/ orderer/ab.proto orderer/cluster.proto

protoc --rust_out=$HOME/src/rust/crypto/fabric-protos-rust/src/peer/ peer/chaincode.proto peer/collection.proto peer/peer.proto peer/proposal_response.proto peer/signed_cc_dep_spec.proto peer/chaincode_event.proto peer/configuration.proto	peer/policy.proto peer/query.proto peer/transaction.proto peer/chaincode_shim.proto peer/events.proto peer/proposal.proto peer/resources.proto
protoc --rust-grpc_out=$HOME/src/rust/crypto/fabric-protos-rust/src/peer/ peer/chaincode_shim.proto peer/peer.proto peer/events.proto
protoc --rust_out=$HOME/src/rust/crypto/fabric-protos-rust/src/peer/lifecycle peer/lifecycle/chaincode_definition.proto peer/lifecycle/lifecycle.proto peer/lifecycle/db.proto

protoc --rust_out=$HOME/src/rust/crypto/fabric-protos-rust/src/transientstore/ transientstore/transientstore.proto
