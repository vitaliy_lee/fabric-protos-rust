// This file is generated by rust-protobuf 2.18.2. Do not edit
// @generated

// https://github.com/rust-lang/rust-clippy/issues/702
#![allow(unknown_lints)]
#![allow(clippy::all)]

#![allow(unused_attributes)]
#![cfg_attr(rustfmt, rustfmt::skip)]

#![allow(box_pointers)]
#![allow(dead_code)]
#![allow(missing_docs)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(non_upper_case_globals)]
#![allow(trivial_casts)]
#![allow(unused_imports)]
#![allow(unused_results)]
//! Generated file from `common/ledger.proto`

/// Generated files are compatible only with the same version
/// of protobuf runtime.
// const _PROTOBUF_VERSION_CHECK: () = ::protobuf::VERSION_2_18_2;

#[derive(PartialEq,Clone,Default)]
pub struct BlockchainInfo {
    // message fields
    pub height: u64,
    pub currentBlockHash: ::std::vec::Vec<u8>,
    pub previousBlockHash: ::std::vec::Vec<u8>,
    pub bootstrappingSnapshotInfo: ::protobuf::SingularPtrField<BootstrappingSnapshotInfo>,
    // special fields
    pub unknown_fields: ::protobuf::UnknownFields,
    pub cached_size: ::protobuf::CachedSize,
}

impl<'a> ::std::default::Default for &'a BlockchainInfo {
    fn default() -> &'a BlockchainInfo {
        <BlockchainInfo as ::protobuf::Message>::default_instance()
    }
}

impl BlockchainInfo {
    pub fn new() -> BlockchainInfo {
        ::std::default::Default::default()
    }

    // uint64 height = 1;


    pub fn get_height(&self) -> u64 {
        self.height
    }
    pub fn clear_height(&mut self) {
        self.height = 0;
    }

    // Param is passed by value, moved
    pub fn set_height(&mut self, v: u64) {
        self.height = v;
    }

    // bytes currentBlockHash = 2;


    pub fn get_currentBlockHash(&self) -> &[u8] {
        &self.currentBlockHash
    }
    pub fn clear_currentBlockHash(&mut self) {
        self.currentBlockHash.clear();
    }

    // Param is passed by value, moved
    pub fn set_currentBlockHash(&mut self, v: ::std::vec::Vec<u8>) {
        self.currentBlockHash = v;
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_currentBlockHash(&mut self) -> &mut ::std::vec::Vec<u8> {
        &mut self.currentBlockHash
    }

    // Take field
    pub fn take_currentBlockHash(&mut self) -> ::std::vec::Vec<u8> {
        ::std::mem::replace(&mut self.currentBlockHash, ::std::vec::Vec::new())
    }

    // bytes previousBlockHash = 3;


    pub fn get_previousBlockHash(&self) -> &[u8] {
        &self.previousBlockHash
    }
    pub fn clear_previousBlockHash(&mut self) {
        self.previousBlockHash.clear();
    }

    // Param is passed by value, moved
    pub fn set_previousBlockHash(&mut self, v: ::std::vec::Vec<u8>) {
        self.previousBlockHash = v;
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_previousBlockHash(&mut self) -> &mut ::std::vec::Vec<u8> {
        &mut self.previousBlockHash
    }

    // Take field
    pub fn take_previousBlockHash(&mut self) -> ::std::vec::Vec<u8> {
        ::std::mem::replace(&mut self.previousBlockHash, ::std::vec::Vec::new())
    }

    // .common.BootstrappingSnapshotInfo bootstrappingSnapshotInfo = 4;


    pub fn get_bootstrappingSnapshotInfo(&self) -> &BootstrappingSnapshotInfo {
        self.bootstrappingSnapshotInfo.as_ref().unwrap_or_else(|| <BootstrappingSnapshotInfo as ::protobuf::Message>::default_instance())
    }
    pub fn clear_bootstrappingSnapshotInfo(&mut self) {
        self.bootstrappingSnapshotInfo.clear();
    }

    pub fn has_bootstrappingSnapshotInfo(&self) -> bool {
        self.bootstrappingSnapshotInfo.is_some()
    }

    // Param is passed by value, moved
    pub fn set_bootstrappingSnapshotInfo(&mut self, v: BootstrappingSnapshotInfo) {
        self.bootstrappingSnapshotInfo = ::protobuf::SingularPtrField::some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_bootstrappingSnapshotInfo(&mut self) -> &mut BootstrappingSnapshotInfo {
        if self.bootstrappingSnapshotInfo.is_none() {
            self.bootstrappingSnapshotInfo.set_default();
        }
        self.bootstrappingSnapshotInfo.as_mut().unwrap()
    }

    // Take field
    pub fn take_bootstrappingSnapshotInfo(&mut self) -> BootstrappingSnapshotInfo {
        self.bootstrappingSnapshotInfo.take().unwrap_or_else(|| BootstrappingSnapshotInfo::new())
    }
}

impl ::protobuf::Message for BlockchainInfo {
    fn is_initialized(&self) -> bool {
        for v in &self.bootstrappingSnapshotInfo {
            if !v.is_initialized() {
                return false;
            }
        };
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream<'_>) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_uint64()?;
                    self.height = tmp;
                },
                2 => {
                    ::protobuf::rt::read_singular_proto3_bytes_into(wire_type, is, &mut self.currentBlockHash)?;
                },
                3 => {
                    ::protobuf::rt::read_singular_proto3_bytes_into(wire_type, is, &mut self.previousBlockHash)?;
                },
                4 => {
                    ::protobuf::rt::read_singular_message_into(wire_type, is, &mut self.bootstrappingSnapshotInfo)?;
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if self.height != 0 {
            my_size += ::protobuf::rt::value_size(1, self.height, ::protobuf::wire_format::WireTypeVarint);
        }
        if !self.currentBlockHash.is_empty() {
            my_size += ::protobuf::rt::bytes_size(2, &self.currentBlockHash);
        }
        if !self.previousBlockHash.is_empty() {
            my_size += ::protobuf::rt::bytes_size(3, &self.previousBlockHash);
        }
        if let Some(ref v) = self.bootstrappingSnapshotInfo.as_ref() {
            let len = v.compute_size();
            my_size += 1 + ::protobuf::rt::compute_raw_varint32_size(len) + len;
        }
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream<'_>) -> ::protobuf::ProtobufResult<()> {
        if self.height != 0 {
            os.write_uint64(1, self.height)?;
        }
        if !self.currentBlockHash.is_empty() {
            os.write_bytes(2, &self.currentBlockHash)?;
        }
        if !self.previousBlockHash.is_empty() {
            os.write_bytes(3, &self.previousBlockHash)?;
        }
        if let Some(ref v) = self.bootstrappingSnapshotInfo.as_ref() {
            os.write_tag(4, ::protobuf::wire_format::WireTypeLengthDelimited)?;
            os.write_raw_varint32(v.get_cached_size())?;
            v.write_to_with_cached_sizes(os)?;
        }
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &dyn (::std::any::Any) {
        self as &dyn (::std::any::Any)
    }
    fn as_any_mut(&mut self) -> &mut dyn (::std::any::Any) {
        self as &mut dyn (::std::any::Any)
    }
    fn into_any(self: ::std::boxed::Box<Self>) -> ::std::boxed::Box<dyn (::std::any::Any)> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        Self::descriptor_static()
    }

    fn new() -> BlockchainInfo {
        BlockchainInfo::new()
    }

    fn descriptor_static() -> &'static ::protobuf::reflect::MessageDescriptor {
        static descriptor: ::protobuf::rt::LazyV2<::protobuf::reflect::MessageDescriptor> = ::protobuf::rt::LazyV2::INIT;
        descriptor.get(|| {
            let mut fields = ::std::vec::Vec::new();
            fields.push(::protobuf::reflect::accessor::make_simple_field_accessor::<_, ::protobuf::types::ProtobufTypeUint64>(
                "height",
                |m: &BlockchainInfo| { &m.height },
                |m: &mut BlockchainInfo| { &mut m.height },
            ));
            fields.push(::protobuf::reflect::accessor::make_simple_field_accessor::<_, ::protobuf::types::ProtobufTypeBytes>(
                "currentBlockHash",
                |m: &BlockchainInfo| { &m.currentBlockHash },
                |m: &mut BlockchainInfo| { &mut m.currentBlockHash },
            ));
            fields.push(::protobuf::reflect::accessor::make_simple_field_accessor::<_, ::protobuf::types::ProtobufTypeBytes>(
                "previousBlockHash",
                |m: &BlockchainInfo| { &m.previousBlockHash },
                |m: &mut BlockchainInfo| { &mut m.previousBlockHash },
            ));
            fields.push(::protobuf::reflect::accessor::make_singular_ptr_field_accessor::<_, ::protobuf::types::ProtobufTypeMessage<BootstrappingSnapshotInfo>>(
                "bootstrappingSnapshotInfo",
                |m: &BlockchainInfo| { &m.bootstrappingSnapshotInfo },
                |m: &mut BlockchainInfo| { &mut m.bootstrappingSnapshotInfo },
            ));
            ::protobuf::reflect::MessageDescriptor::new_pb_name::<BlockchainInfo>(
                "BlockchainInfo",
                fields,
                file_descriptor_proto()
            )
        })
    }

    fn default_instance() -> &'static BlockchainInfo {
        static instance: ::protobuf::rt::LazyV2<BlockchainInfo> = ::protobuf::rt::LazyV2::INIT;
        instance.get(BlockchainInfo::new)
    }
}

impl ::protobuf::Clear for BlockchainInfo {
    fn clear(&mut self) {
        self.height = 0;
        self.currentBlockHash.clear();
        self.previousBlockHash.clear();
        self.bootstrappingSnapshotInfo.clear();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for BlockchainInfo {
    fn fmt(&self, f: &mut ::std::fmt::Formatter<'_>) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for BlockchainInfo {
    fn as_ref(&self) -> ::protobuf::reflect::ReflectValueRef {
        ::protobuf::reflect::ReflectValueRef::Message(self)
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct BootstrappingSnapshotInfo {
    // message fields
    pub lastBlockInSnapshot: u64,
    // special fields
    pub unknown_fields: ::protobuf::UnknownFields,
    pub cached_size: ::protobuf::CachedSize,
}

impl<'a> ::std::default::Default for &'a BootstrappingSnapshotInfo {
    fn default() -> &'a BootstrappingSnapshotInfo {
        <BootstrappingSnapshotInfo as ::protobuf::Message>::default_instance()
    }
}

impl BootstrappingSnapshotInfo {
    pub fn new() -> BootstrappingSnapshotInfo {
        ::std::default::Default::default()
    }

    // uint64 lastBlockInSnapshot = 1;


    pub fn get_lastBlockInSnapshot(&self) -> u64 {
        self.lastBlockInSnapshot
    }
    pub fn clear_lastBlockInSnapshot(&mut self) {
        self.lastBlockInSnapshot = 0;
    }

    // Param is passed by value, moved
    pub fn set_lastBlockInSnapshot(&mut self, v: u64) {
        self.lastBlockInSnapshot = v;
    }
}

impl ::protobuf::Message for BootstrappingSnapshotInfo {
    fn is_initialized(&self) -> bool {
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream<'_>) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    let tmp = is.read_uint64()?;
                    self.lastBlockInSnapshot = tmp;
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if self.lastBlockInSnapshot != 0 {
            my_size += ::protobuf::rt::value_size(1, self.lastBlockInSnapshot, ::protobuf::wire_format::WireTypeVarint);
        }
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream<'_>) -> ::protobuf::ProtobufResult<()> {
        if self.lastBlockInSnapshot != 0 {
            os.write_uint64(1, self.lastBlockInSnapshot)?;
        }
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &dyn (::std::any::Any) {
        self as &dyn (::std::any::Any)
    }
    fn as_any_mut(&mut self) -> &mut dyn (::std::any::Any) {
        self as &mut dyn (::std::any::Any)
    }
    fn into_any(self: ::std::boxed::Box<Self>) -> ::std::boxed::Box<dyn (::std::any::Any)> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        Self::descriptor_static()
    }

    fn new() -> BootstrappingSnapshotInfo {
        BootstrappingSnapshotInfo::new()
    }

    fn descriptor_static() -> &'static ::protobuf::reflect::MessageDescriptor {
        static descriptor: ::protobuf::rt::LazyV2<::protobuf::reflect::MessageDescriptor> = ::protobuf::rt::LazyV2::INIT;
        descriptor.get(|| {
            let mut fields = ::std::vec::Vec::new();
            fields.push(::protobuf::reflect::accessor::make_simple_field_accessor::<_, ::protobuf::types::ProtobufTypeUint64>(
                "lastBlockInSnapshot",
                |m: &BootstrappingSnapshotInfo| { &m.lastBlockInSnapshot },
                |m: &mut BootstrappingSnapshotInfo| { &mut m.lastBlockInSnapshot },
            ));
            ::protobuf::reflect::MessageDescriptor::new_pb_name::<BootstrappingSnapshotInfo>(
                "BootstrappingSnapshotInfo",
                fields,
                file_descriptor_proto()
            )
        })
    }

    fn default_instance() -> &'static BootstrappingSnapshotInfo {
        static instance: ::protobuf::rt::LazyV2<BootstrappingSnapshotInfo> = ::protobuf::rt::LazyV2::INIT;
        instance.get(BootstrappingSnapshotInfo::new)
    }
}

impl ::protobuf::Clear for BootstrappingSnapshotInfo {
    fn clear(&mut self) {
        self.lastBlockInSnapshot = 0;
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for BootstrappingSnapshotInfo {
    fn fmt(&self, f: &mut ::std::fmt::Formatter<'_>) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for BootstrappingSnapshotInfo {
    fn as_ref(&self) -> ::protobuf::reflect::ReflectValueRef {
        ::protobuf::reflect::ReflectValueRef::Message(self)
    }
}

static file_descriptor_proto_data: &'static [u8] = b"\
    \n\x13common/ledger.proto\x12\x06common\"\xe3\x01\n\x0eBlockchainInfo\
    \x12\x16\n\x06height\x18\x01\x20\x01(\x04R\x06height\x12*\n\x10currentBl\
    ockHash\x18\x02\x20\x01(\x0cR\x10currentBlockHash\x12,\n\x11previousBloc\
    kHash\x18\x03\x20\x01(\x0cR\x11previousBlockHash\x12_\n\x19bootstrapping\
    SnapshotInfo\x18\x04\x20\x01(\x0b2!.common.BootstrappingSnapshotInfoR\
    \x19bootstrappingSnapshotInfo\"M\n\x19BootstrappingSnapshotInfo\x120\n\
    \x13lastBlockInSnapshot\x18\x01\x20\x01(\x04R\x13lastBlockInSnapshotBV\n\
    $org.hyperledger.fabric.protos.commonZ.github.com/hyperledger/fabric-pro\
    tos-go/commonJ\xba\x06\n\x06\x12\x04\x04\0\x19\x01\nu\n\x01\x0c\x12\x03\
    \x04\0\x122k\x20Copyright\x20the\x20Hyperledger\x20Fabric\x20contributor\
    s.\x20All\x20rights\x20reserved.\n\n\x20SPDX-License-Identifier:\x20Apac\
    he-2.0\n\n\x08\n\x01\x08\x12\x03\x06\0E\n\t\n\x02\x08\x0b\x12\x03\x06\0E\
    \n\x08\n\x01\x08\x12\x03\x07\0=\n\t\n\x02\x08\x01\x12\x03\x07\0=\n\x08\n\
    \x01\x02\x12\x03\t\0\x0f\n|\n\x02\x04\0\x12\x04\r\0\x14\x01\x1ap\x20Cont\
    ains\x20information\x20about\x20the\x20blockchain\x20ledger\x20such\x20a\
    s\x20height,\x20current\n\x20block\x20hash,\x20and\x20previous\x20block\
    \x20hash.\n\n\n\n\x03\x04\0\x01\x12\x03\r\x08\x16\n\x0b\n\x04\x04\0\x02\
    \0\x12\x03\x0e\x04\x16\n\x0c\n\x05\x04\0\x02\0\x05\x12\x03\x0e\x04\n\n\
    \x0c\n\x05\x04\0\x02\0\x01\x12\x03\x0e\x0b\x11\n\x0c\n\x05\x04\0\x02\0\
    \x03\x12\x03\x0e\x14\x15\n\x0b\n\x04\x04\0\x02\x01\x12\x03\x0f\x04\x1f\n\
    \x0c\n\x05\x04\0\x02\x01\x05\x12\x03\x0f\x04\t\n\x0c\n\x05\x04\0\x02\x01\
    \x01\x12\x03\x0f\n\x1a\n\x0c\n\x05\x04\0\x02\x01\x03\x12\x03\x0f\x1d\x1e\
    \n\x0b\n\x04\x04\0\x02\x02\x12\x03\x10\x04\x20\n\x0c\n\x05\x04\0\x02\x02\
    \x05\x12\x03\x10\x04\t\n\x0c\n\x05\x04\0\x02\x02\x01\x12\x03\x10\n\x1b\n\
    \x0c\n\x05\x04\0\x02\x02\x03\x12\x03\x10\x1e\x1f\n\xa4\x01\n\x04\x04\0\
    \x02\x03\x12\x03\x13\x04<\x1a\x96\x01\x20Specifies\x20bootstrapping\x20s\
    napshot\x20info\x20if\x20the\x20channel\x20is\x20bootstrapped\x20from\
    \x20a\x20snapshot.\n\x20It\x20is\x20nil\x20if\x20the\x20channel\x20is\
    \x20not\x20bootstrapped\x20from\x20a\x20snapshot.\n\n\x0c\n\x05\x04\0\
    \x02\x03\x06\x12\x03\x13\x04\x1d\n\x0c\n\x05\x04\0\x02\x03\x01\x12\x03\
    \x13\x1e7\n\x0c\n\x05\x04\0\x02\x03\x03\x12\x03\x13:;\nB\n\x02\x04\x01\
    \x12\x04\x17\0\x19\x01\x1a6\x20Contains\x20information\x20for\x20the\x20\
    bootstrapping\x20snapshot.\n\n\n\n\x03\x04\x01\x01\x12\x03\x17\x08!\n\
    \x0b\n\x04\x04\x01\x02\0\x12\x03\x18\x04#\n\x0c\n\x05\x04\x01\x02\0\x05\
    \x12\x03\x18\x04\n\n\x0c\n\x05\x04\x01\x02\0\x01\x12\x03\x18\x0b\x1e\n\
    \x0c\n\x05\x04\x01\x02\0\x03\x12\x03\x18!\"b\x06proto3\
";

static file_descriptor_proto_lazy: ::protobuf::rt::LazyV2<::protobuf::descriptor::FileDescriptorProto> = ::protobuf::rt::LazyV2::INIT;

fn parse_descriptor_proto() -> ::protobuf::descriptor::FileDescriptorProto {
    ::protobuf::parse_from_bytes(file_descriptor_proto_data).unwrap()
}

pub fn file_descriptor_proto() -> &'static ::protobuf::descriptor::FileDescriptorProto {
    file_descriptor_proto_lazy.get(|| {
        parse_descriptor_proto()
    })
}
