// This file is generated by rust-protobuf 2.18.2. Do not edit
// @generated

// https://github.com/rust-lang/rust-clippy/issues/702
#![allow(unknown_lints)]
#![allow(clippy::all)]

#![allow(unused_attributes)]
#![cfg_attr(rustfmt, rustfmt::skip)]

#![allow(box_pointers)]
#![allow(dead_code)]
#![allow(missing_docs)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(non_upper_case_globals)]
#![allow(trivial_casts)]
#![allow(unused_imports)]
#![allow(unused_results)]
//! Generated file from `peer/lifecycle/db.proto`

/// Generated files are compatible only with the same version
/// of protobuf runtime.
// const _PROTOBUF_VERSION_CHECK: () = ::protobuf::VERSION_2_18_2;

#[derive(PartialEq,Clone,Default)]
pub struct StateMetadata {
    // message fields
    pub datatype: ::std::string::String,
    pub fields: ::protobuf::RepeatedField<::std::string::String>,
    // special fields
    pub unknown_fields: ::protobuf::UnknownFields,
    pub cached_size: ::protobuf::CachedSize,
}

impl<'a> ::std::default::Default for &'a StateMetadata {
    fn default() -> &'a StateMetadata {
        <StateMetadata as ::protobuf::Message>::default_instance()
    }
}

impl StateMetadata {
    pub fn new() -> StateMetadata {
        ::std::default::Default::default()
    }

    // string datatype = 1;


    pub fn get_datatype(&self) -> &str {
        &self.datatype
    }
    pub fn clear_datatype(&mut self) {
        self.datatype.clear();
    }

    // Param is passed by value, moved
    pub fn set_datatype(&mut self, v: ::std::string::String) {
        self.datatype = v;
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_datatype(&mut self) -> &mut ::std::string::String {
        &mut self.datatype
    }

    // Take field
    pub fn take_datatype(&mut self) -> ::std::string::String {
        ::std::mem::replace(&mut self.datatype, ::std::string::String::new())
    }

    // repeated string fields = 2;


    pub fn get_fields(&self) -> &[::std::string::String] {
        &self.fields
    }
    pub fn clear_fields(&mut self) {
        self.fields.clear();
    }

    // Param is passed by value, moved
    pub fn set_fields(&mut self, v: ::protobuf::RepeatedField<::std::string::String>) {
        self.fields = v;
    }

    // Mutable pointer to the field.
    pub fn mut_fields(&mut self) -> &mut ::protobuf::RepeatedField<::std::string::String> {
        &mut self.fields
    }

    // Take field
    pub fn take_fields(&mut self) -> ::protobuf::RepeatedField<::std::string::String> {
        ::std::mem::replace(&mut self.fields, ::protobuf::RepeatedField::new())
    }
}

impl ::protobuf::Message for StateMetadata {
    fn is_initialized(&self) -> bool {
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream<'_>) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    ::protobuf::rt::read_singular_proto3_string_into(wire_type, is, &mut self.datatype)?;
                },
                2 => {
                    ::protobuf::rt::read_repeated_string_into(wire_type, is, &mut self.fields)?;
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if !self.datatype.is_empty() {
            my_size += ::protobuf::rt::string_size(1, &self.datatype);
        }
        for value in &self.fields {
            my_size += ::protobuf::rt::string_size(2, &value);
        };
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream<'_>) -> ::protobuf::ProtobufResult<()> {
        if !self.datatype.is_empty() {
            os.write_string(1, &self.datatype)?;
        }
        for v in &self.fields {
            os.write_string(2, &v)?;
        };
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &dyn (::std::any::Any) {
        self as &dyn (::std::any::Any)
    }
    fn as_any_mut(&mut self) -> &mut dyn (::std::any::Any) {
        self as &mut dyn (::std::any::Any)
    }
    fn into_any(self: ::std::boxed::Box<Self>) -> ::std::boxed::Box<dyn (::std::any::Any)> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        Self::descriptor_static()
    }

    fn new() -> StateMetadata {
        StateMetadata::new()
    }

    fn descriptor_static() -> &'static ::protobuf::reflect::MessageDescriptor {
        static descriptor: ::protobuf::rt::LazyV2<::protobuf::reflect::MessageDescriptor> = ::protobuf::rt::LazyV2::INIT;
        descriptor.get(|| {
            let mut fields = ::std::vec::Vec::new();
            fields.push(::protobuf::reflect::accessor::make_simple_field_accessor::<_, ::protobuf::types::ProtobufTypeString>(
                "datatype",
                |m: &StateMetadata| { &m.datatype },
                |m: &mut StateMetadata| { &mut m.datatype },
            ));
            fields.push(::protobuf::reflect::accessor::make_repeated_field_accessor::<_, ::protobuf::types::ProtobufTypeString>(
                "fields",
                |m: &StateMetadata| { &m.fields },
                |m: &mut StateMetadata| { &mut m.fields },
            ));
            ::protobuf::reflect::MessageDescriptor::new_pb_name::<StateMetadata>(
                "StateMetadata",
                fields,
                file_descriptor_proto()
            )
        })
    }

    fn default_instance() -> &'static StateMetadata {
        static instance: ::protobuf::rt::LazyV2<StateMetadata> = ::protobuf::rt::LazyV2::INIT;
        instance.get(StateMetadata::new)
    }
}

impl ::protobuf::Clear for StateMetadata {
    fn clear(&mut self) {
        self.datatype.clear();
        self.fields.clear();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for StateMetadata {
    fn fmt(&self, f: &mut ::std::fmt::Formatter<'_>) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for StateMetadata {
    fn as_ref(&self) -> ::protobuf::reflect::ReflectValueRef {
        ::protobuf::reflect::ReflectValueRef::Message(self)
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct StateData {
    // message oneof groups
    pub Type: ::std::option::Option<StateData_oneof_Type>,
    // special fields
    pub unknown_fields: ::protobuf::UnknownFields,
    pub cached_size: ::protobuf::CachedSize,
}

impl<'a> ::std::default::Default for &'a StateData {
    fn default() -> &'a StateData {
        <StateData as ::protobuf::Message>::default_instance()
    }
}

#[derive(Clone,PartialEq,Debug)]
pub enum StateData_oneof_Type {
    Int64(i64),
    Bytes(::std::vec::Vec<u8>),
    String(::std::string::String),
}

impl StateData {
    pub fn new() -> StateData {
        ::std::default::Default::default()
    }

    // int64 Int64 = 1;


    pub fn get_Int64(&self) -> i64 {
        match self.Type {
            ::std::option::Option::Some(StateData_oneof_Type::Int64(v)) => v,
            _ => 0,
        }
    }
    pub fn clear_Int64(&mut self) {
        self.Type = ::std::option::Option::None;
    }

    pub fn has_Int64(&self) -> bool {
        match self.Type {
            ::std::option::Option::Some(StateData_oneof_Type::Int64(..)) => true,
            _ => false,
        }
    }

    // Param is passed by value, moved
    pub fn set_Int64(&mut self, v: i64) {
        self.Type = ::std::option::Option::Some(StateData_oneof_Type::Int64(v))
    }

    // bytes Bytes = 2;


    pub fn get_Bytes(&self) -> &[u8] {
        match self.Type {
            ::std::option::Option::Some(StateData_oneof_Type::Bytes(ref v)) => v,
            _ => &[],
        }
    }
    pub fn clear_Bytes(&mut self) {
        self.Type = ::std::option::Option::None;
    }

    pub fn has_Bytes(&self) -> bool {
        match self.Type {
            ::std::option::Option::Some(StateData_oneof_Type::Bytes(..)) => true,
            _ => false,
        }
    }

    // Param is passed by value, moved
    pub fn set_Bytes(&mut self, v: ::std::vec::Vec<u8>) {
        self.Type = ::std::option::Option::Some(StateData_oneof_Type::Bytes(v))
    }

    // Mutable pointer to the field.
    pub fn mut_Bytes(&mut self) -> &mut ::std::vec::Vec<u8> {
        if let ::std::option::Option::Some(StateData_oneof_Type::Bytes(_)) = self.Type {
        } else {
            self.Type = ::std::option::Option::Some(StateData_oneof_Type::Bytes(::std::vec::Vec::new()));
        }
        match self.Type {
            ::std::option::Option::Some(StateData_oneof_Type::Bytes(ref mut v)) => v,
            _ => panic!(),
        }
    }

    // Take field
    pub fn take_Bytes(&mut self) -> ::std::vec::Vec<u8> {
        if self.has_Bytes() {
            match self.Type.take() {
                ::std::option::Option::Some(StateData_oneof_Type::Bytes(v)) => v,
                _ => panic!(),
            }
        } else {
            ::std::vec::Vec::new()
        }
    }

    // string String = 3;


    pub fn get_String(&self) -> &str {
        match self.Type {
            ::std::option::Option::Some(StateData_oneof_Type::String(ref v)) => v,
            _ => "",
        }
    }
    pub fn clear_String(&mut self) {
        self.Type = ::std::option::Option::None;
    }

    pub fn has_String(&self) -> bool {
        match self.Type {
            ::std::option::Option::Some(StateData_oneof_Type::String(..)) => true,
            _ => false,
        }
    }

    // Param is passed by value, moved
    pub fn set_String(&mut self, v: ::std::string::String) {
        self.Type = ::std::option::Option::Some(StateData_oneof_Type::String(v))
    }

    // Mutable pointer to the field.
    pub fn mut_String(&mut self) -> &mut ::std::string::String {
        if let ::std::option::Option::Some(StateData_oneof_Type::String(_)) = self.Type {
        } else {
            self.Type = ::std::option::Option::Some(StateData_oneof_Type::String(::std::string::String::new()));
        }
        match self.Type {
            ::std::option::Option::Some(StateData_oneof_Type::String(ref mut v)) => v,
            _ => panic!(),
        }
    }

    // Take field
    pub fn take_String(&mut self) -> ::std::string::String {
        if self.has_String() {
            match self.Type.take() {
                ::std::option::Option::Some(StateData_oneof_Type::String(v)) => v,
                _ => panic!(),
            }
        } else {
            ::std::string::String::new()
        }
    }
}

impl ::protobuf::Message for StateData {
    fn is_initialized(&self) -> bool {
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream<'_>) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    self.Type = ::std::option::Option::Some(StateData_oneof_Type::Int64(is.read_int64()?));
                },
                2 => {
                    if wire_type != ::protobuf::wire_format::WireTypeLengthDelimited {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    self.Type = ::std::option::Option::Some(StateData_oneof_Type::Bytes(is.read_bytes()?));
                },
                3 => {
                    if wire_type != ::protobuf::wire_format::WireTypeLengthDelimited {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    }
                    self.Type = ::std::option::Option::Some(StateData_oneof_Type::String(is.read_string()?));
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if let ::std::option::Option::Some(ref v) = self.Type {
            match v {
                &StateData_oneof_Type::Int64(v) => {
                    my_size += ::protobuf::rt::value_size(1, v, ::protobuf::wire_format::WireTypeVarint);
                },
                &StateData_oneof_Type::Bytes(ref v) => {
                    my_size += ::protobuf::rt::bytes_size(2, &v);
                },
                &StateData_oneof_Type::String(ref v) => {
                    my_size += ::protobuf::rt::string_size(3, &v);
                },
            };
        }
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream<'_>) -> ::protobuf::ProtobufResult<()> {
        if let ::std::option::Option::Some(ref v) = self.Type {
            match v {
                &StateData_oneof_Type::Int64(v) => {
                    os.write_int64(1, v)?;
                },
                &StateData_oneof_Type::Bytes(ref v) => {
                    os.write_bytes(2, v)?;
                },
                &StateData_oneof_Type::String(ref v) => {
                    os.write_string(3, v)?;
                },
            };
        }
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &dyn (::std::any::Any) {
        self as &dyn (::std::any::Any)
    }
    fn as_any_mut(&mut self) -> &mut dyn (::std::any::Any) {
        self as &mut dyn (::std::any::Any)
    }
    fn into_any(self: ::std::boxed::Box<Self>) -> ::std::boxed::Box<dyn (::std::any::Any)> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        Self::descriptor_static()
    }

    fn new() -> StateData {
        StateData::new()
    }

    fn descriptor_static() -> &'static ::protobuf::reflect::MessageDescriptor {
        static descriptor: ::protobuf::rt::LazyV2<::protobuf::reflect::MessageDescriptor> = ::protobuf::rt::LazyV2::INIT;
        descriptor.get(|| {
            let mut fields = ::std::vec::Vec::new();
            fields.push(::protobuf::reflect::accessor::make_singular_i64_accessor::<_>(
                "Int64",
                StateData::has_Int64,
                StateData::get_Int64,
            ));
            fields.push(::protobuf::reflect::accessor::make_singular_bytes_accessor::<_>(
                "Bytes",
                StateData::has_Bytes,
                StateData::get_Bytes,
            ));
            fields.push(::protobuf::reflect::accessor::make_singular_string_accessor::<_>(
                "String",
                StateData::has_String,
                StateData::get_String,
            ));
            ::protobuf::reflect::MessageDescriptor::new_pb_name::<StateData>(
                "StateData",
                fields,
                file_descriptor_proto()
            )
        })
    }

    fn default_instance() -> &'static StateData {
        static instance: ::protobuf::rt::LazyV2<StateData> = ::protobuf::rt::LazyV2::INIT;
        instance.get(StateData::new)
    }
}

impl ::protobuf::Clear for StateData {
    fn clear(&mut self) {
        self.Type = ::std::option::Option::None;
        self.Type = ::std::option::Option::None;
        self.Type = ::std::option::Option::None;
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for StateData {
    fn fmt(&self, f: &mut ::std::fmt::Formatter<'_>) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for StateData {
    fn as_ref(&self) -> ::protobuf::reflect::ReflectValueRef {
        ::protobuf::reflect::ReflectValueRef::Message(self)
    }
}

static file_descriptor_proto_data: &'static [u8] = b"\
    \n\x17peer/lifecycle/db.proto\x12\tlifecycle\"C\n\rStateMetadata\x12\x1a\
    \n\x08datatype\x18\x01\x20\x01(\tR\x08datatype\x12\x16\n\x06fields\x18\
    \x02\x20\x03(\tR\x06fields\"]\n\tStateData\x12\x16\n\x05Int64\x18\x01\
    \x20\x01(\x03H\0R\x05Int64\x12\x16\n\x05Bytes\x18\x02\x20\x01(\x0cH\0R\
    \x05Bytes\x12\x18\n\x06String\x18\x03\x20\x01(\tH\0R\x06StringB\x06\n\
    \x04TypeBf\n,org.hyperledger.fabric.protos.peer.lifecycleZ6github.com/hy\
    perledger/fabric-protos-go/peer/lifecycleJ\xb4\x07\n\x06\x12\x04\x04\0\
    \x1d\x01\nu\n\x01\x0c\x12\x03\x04\0\x122k\x20Copyright\x20the\x20Hyperle\
    dger\x20Fabric\x20contributors.\x20All\x20rights\x20reserved.\n\n\x20SPD\
    X-License-Identifier:\x20Apache-2.0\n\n\x08\n\x01\x08\x12\x03\x06\0E\n\t\
    \n\x02\x08\x01\x12\x03\x06\0E\n\x08\n\x01\x08\x12\x03\x07\0M\n\t\n\x02\
    \x08\x0b\x12\x03\x07\0M\n\x08\n\x01\x02\x12\x03\t\0\x12\n\xe7\x02\n\x02\
    \x04\0\x12\x04\x11\0\x14\x01\x1a\xd9\x01\x20StateMetadata\x20describes\
    \x20the\x20keys\x20in\x20a\x20namespace.\x20\x20It\x20is\x20necessary\
    \x20because\n\x20in\x20collections,\x20range\x20scans\x20are\x20not\x20p\
    ossible\x20during\x20transactions\x20which\n\x20write.\x20\x20Therefore\
    \x20we\x20must\x20track\x20the\x20keys\x20in\x20our\x20namespace\x20ours\
    elves.\n2\x7f\x20These\x20protos\x20are\x20used\x20for\x20encoding\x20da\
    ta\x20into\x20the\x20statedb\n\x20in\x20general,\x20it\x20should\x20not\
    \x20be\x20necessary\x20for\x20clients\x20to\x20utilize\x20them.\n\n\n\n\
    \x03\x04\0\x01\x12\x03\x11\x08\x15\n\x0b\n\x04\x04\0\x02\0\x12\x03\x12\
    \x04\x18\n\x0c\n\x05\x04\0\x02\0\x05\x12\x03\x12\x04\n\n\x0c\n\x05\x04\0\
    \x02\0\x01\x12\x03\x12\x0b\x13\n\x0c\n\x05\x04\0\x02\0\x03\x12\x03\x12\
    \x16\x17\n\x0b\n\x04\x04\0\x02\x01\x12\x03\x13\x04\x1f\n\x0c\n\x05\x04\0\
    \x02\x01\x04\x12\x03\x13\x04\x0c\n\x0c\n\x05\x04\0\x02\x01\x05\x12\x03\
    \x13\r\x13\n\x0c\n\x05\x04\0\x02\x01\x01\x12\x03\x13\x14\x1a\n\x0c\n\x05\
    \x04\0\x02\x01\x03\x12\x03\x13\x1d\x1e\n@\n\x02\x04\x01\x12\x04\x17\0\
    \x1d\x01\x1a4\x20StateData\x20encodes\x20a\x20particular\x20field\x20of\
    \x20a\x20datatype\n\n\n\n\x03\x04\x01\x01\x12\x03\x17\x08\x11\n\x0c\n\
    \x04\x04\x01\x08\0\x12\x04\x18\x04\x1c\x05\n\x0c\n\x05\x04\x01\x08\0\x01\
    \x12\x03\x18\n\x0e\n\x0b\n\x04\x04\x01\x02\0\x12\x03\x19\x08\x18\n\x0c\n\
    \x05\x04\x01\x02\0\x05\x12\x03\x19\x08\r\n\x0c\n\x05\x04\x01\x02\0\x01\
    \x12\x03\x19\x0e\x13\n\x0c\n\x05\x04\x01\x02\0\x03\x12\x03\x19\x16\x17\n\
    \x0b\n\x04\x04\x01\x02\x01\x12\x03\x1a\x08\x18\n\x0c\n\x05\x04\x01\x02\
    \x01\x05\x12\x03\x1a\x08\r\n\x0c\n\x05\x04\x01\x02\x01\x01\x12\x03\x1a\
    \x0e\x13\n\x0c\n\x05\x04\x01\x02\x01\x03\x12\x03\x1a\x16\x17\n\x0b\n\x04\
    \x04\x01\x02\x02\x12\x03\x1b\x08\x1a\n\x0c\n\x05\x04\x01\x02\x02\x05\x12\
    \x03\x1b\x08\x0e\n\x0c\n\x05\x04\x01\x02\x02\x01\x12\x03\x1b\x0f\x15\n\
    \x0c\n\x05\x04\x01\x02\x02\x03\x12\x03\x1b\x18\x19b\x06proto3\
";

static file_descriptor_proto_lazy: ::protobuf::rt::LazyV2<::protobuf::descriptor::FileDescriptorProto> = ::protobuf::rt::LazyV2::INIT;

fn parse_descriptor_proto() -> ::protobuf::descriptor::FileDescriptorProto {
    ::protobuf::parse_from_bytes(file_descriptor_proto_data).unwrap()
}

pub fn file_descriptor_proto() -> &'static ::protobuf::descriptor::FileDescriptorProto {
    file_descriptor_proto_lazy.get(|| {
        parse_descriptor_proto()
    })
}
